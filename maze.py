import numpy as np
import random

def buildMaze (n):
    emptyMaze = [[0 for x in range(n)] for y in range(n)]
    for i in range(int(n/1.15)):
        col = random.randrange(0, n, 1)
        for j in range(int(n/1.15)):
            row = random.randrange(0, n, 1)
            if emptyMaze[col][row] == 0:
                emptyMaze[col][row] = 1
    return emptyMaze

if __name__ == "__main__":
    n = int(input("Enter size of maze: "))
    print(np.matrix(buildMaze(n)))
        
    
